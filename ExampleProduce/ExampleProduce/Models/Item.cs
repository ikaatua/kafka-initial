﻿using Avro;
using Avro.Specific;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleProduce.Models
{
    public class Item : ISpecificRecord
    {
        public static Schema _SCHEMA = Schema.Parse(@"{""type"":""record"",""name"":""Item"",""namespace"":""ExampleProduce.Models"",""fields"":[{""name"":""id"",""type"":""int""},{""name"":""name"",""type"":""string""}]}");
        public int _id;
        public string _name { get; set; }
        //public string _lastname { get; set; }
        public virtual Schema Schema
        {
            get
            {
                return Item._SCHEMA;
            }
        }
        public int id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }

        public string name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        //public string lastname
        //{
        //    get
        //    {
        //        return this._lastname;
        //    }
        //    set
        //    {
        //        this._lastname = value;
        //    }
        //}

        public virtual object Get(int fieldPos)
        {
            switch (fieldPos)
            {
                case 0: return this.id;
                case 1: return this.name;
                default: throw new AvroRuntimeException("Bad index " + fieldPos + " in Get()");
            };
        }

        public virtual void Put(int fieldPos, object fieldValue)
        {
            switch (fieldPos)
            {
                case 0: this.id = (System.Int32)fieldValue; break;
                case 1: this.name = (System.String)fieldValue; break;
                default: throw new AvroRuntimeException("Bad index " + fieldPos + " in Put()");
            };
        }
    }
}
