﻿using Avro;
using Avro.Specific;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleProduce.Models
{
    public class MyHeader
    {
        public static Schema _SCHEMA = Schema.Parse(@"{""type"":""record"",""name"":""MyHeader"",""namespace"":""ExampleProduce.Models"",""fields"":[{""name"":""id"",""type"":""int""},{""name"":""name"",""type"":""string""}]}");
        public int id { get; set; }
        public string name { get; set; }
        public virtual Schema Schema
        {
            get
            {
                return MyHeader._SCHEMA;
            }
        }

        //public virtual object Get(int fieldPos)
        //{
        //    switch (fieldPos)
        //    {
        //        case 0: return this.id;
        //        case 1: return this.name;
        //        default: throw new AvroRuntimeException("Bad index " + fieldPos + " in Get()");
        //    };
        //}

        //public virtual void Put(int fieldPos, object fieldValue)
        //{
        //    switch (fieldPos)
        //    {
        //        case 0: this.id = (System.Int32)fieldValue; break;
        //        case 1: this.name = (System.String)fieldValue; break;
        //        default: throw new AvroRuntimeException("Bad index " + fieldPos + " in Put()");
        //    };
        //}
    }
}
