﻿using Confluent.Kafka;
using ExampleProduce.Models;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Chr.Avro.Confluent;
using Confluent.SchemaRegistry;
using System.Collections.Generic;
using Avro.Generic;
using Confluent.SchemaRegistry.Serdes;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;

namespace ExampleProduce
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            var config = new ProducerConfig { BootstrapServers = "localhost:9092"};
            using (var p = new ProducerBuilder<string, string>(config).Build())
            {
                try
                {
                    for (int i = 0; i < 5; ++i)
                    {
                        var item = new MyHeader { id = i + 1, name = $"Test {i + 1}" };
                        var dr = p.ProduceAsync("example-topic-header", new Message<string, string> { Value = JsonConvert.SerializeObject(item), Key = Guid.NewGuid().ToString() }).Result;
                    }
                }
                catch (ProduceException<string, string> e)
                {
                    Console.WriteLine($"Delivery failed: {e.Error.Reason}");
                }
            }

            //using (var schemaRegistry = new CachedSchemaRegistryClient(new SchemaRegistryConfig { Url = "http://localhost:8081" }))
            //using (var producer =
            //    new ProducerBuilder<string, MyHeader>(new ProducerConfig { BootstrapServers = "localhost:9092" })
            //    //.SetAvroValueSerializer(schemaRegistry, registerAutomatically: AutomaticRegistrationBehavior.Always)
            //    .SetValueSerializer(new AvroSerializer<MyHeader>(schemaRegistry))
            //    .Build())
            //{
            //    for (int i = 0; i < 5; ++i)
            //    {
            //        var item = new MyHeader { id = i + 1, name = $"Test {i + 1}" };
            //        var dr = await producer.ProduceAsync("example-topic-header",
            //            new Message<string, MyHeader>
            //            {
            //                Value = item,
            //                Key = Guid.NewGuid().ToString()
            //            });
            //        producer.Flush(TimeSpan.FromSeconds(30));
            //    }
            //}
        }

    }
}
